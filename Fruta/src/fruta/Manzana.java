/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fruta;

/**
 *
 * @author admin
 */
public class Manzana extends Fruta {
    private double pr;
    private int p;

    public Manzana(double pr, String nombre, int p) {
        super(nombre);
        this.pr = pr;
        this.p = p;
    }

  

    public double getPr() {
        return pr;
    }


    public void setPr(double pr) {
        this.pr = pr;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }
  
    

   

    @Override
    public void precio() {
         System.out.println("El precio de la " + nombre + " es: " + pr +" y lleva " + p + " "+ nombre);
        
    }

}
